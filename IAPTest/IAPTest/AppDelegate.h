//
//  AppDelegate.h
//  IAPTest
//
//  Created by Vlad Yalovenko on 06/12/2016.
//  Copyright © 2016 Vlad Yalovenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

