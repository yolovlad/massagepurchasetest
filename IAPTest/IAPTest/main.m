//
//  main.m
//  IAPTest
//
//  Created by Vlad Yalovenko on 06/12/2016.
//  Copyright © 2016 Vlad Yalovenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
